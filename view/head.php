<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title> Eestimaa jookseb 2015 </title>
		<link type="text/css"  rel="stylesheet" href="css/stiil.css" />
	</head>
	<body>
	    <?php session_start(); ?>
		<div id="header">
			<ul id ="esiriba">
			
				<?php if(isset($_SESSION["logitud"]) &&  !empty($_SESSION["logitud"])) :   ?>

				<li class ="esiriba"><a href ="?mode=avaleht">Avaleht</a></li>
				<li class = "esiriba"><a href ="?mode=uritused">Üritused</a></li>
				<li class = "esiriba"><a href ="?mode=tulemused">Tulemused</a></li>
				<li class ="esiriba"><a href ="?mode=galerii">Galerii</a></li>
				<li class ="esiriba"><a href ="?mode=kontakt">Kontakt</a></li>
				
				<li class ="esiriba"><a href ="?mode=andmed">Andmed</a></li>
				<li class = "esiriba"><a href ="?mode=jooksud">Jooksud</a></li>
				<li class = "esiriba"><a href ="?mode=pildid">Pildid</a></li>
				<li class ="esiriba"><a href ="?mode=core/logout">Logout</a></li>

				<?php else : ?> 
				
				<li class ="esiriba"><a href ="?mode=avaleht">Avaleht</a></li>
				<li class = "esiriba"><a href ="?mode=uritused">Üritused</a></li>
				<li class = "esiriba"><a href ="?mode=tulemused">Tulemused</a></li>
				<li class ="esiriba"><a href ="?mode=galerii">Galerii</a></li>
				<li class ="esiriba"><a href ="?mode=kontakt">Kontakt</a></li>
				<?php endif; ?>
			</ul>
        
