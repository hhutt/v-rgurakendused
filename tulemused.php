
<?php 
    if(isset($_GET['koht'])) {
        $koht = $_GET['koht'];
    } else {
        $koht = "";
    }
?>
<?php if($koht=="tartu") : ?>
            <h1> 1. Tartu Jooksu Tulemused </h1>
		</div>
		<div id= "section">
			<table>
				<col width="130"/>
				<col width="200"/>
				<col width="200"/>
				<col width="200"/>
				<tr>
					<th> Võistleja nr. </th><th> Nimi </th><th> Elukoht </th><th> Finišh </th>
				</tr>
				<tr>
					<td> 1 </td><td> Helen </td><td> Tartu </td><td> 00:56:08 </td>
				</tr>
				<tr>
					<td> 2 </td><td> Triin </td><td> Tartu </td><td> 00:58:32 </td>
				</tr>
				<tr>
					<td> 3 </td><td> Sten </td><td> Tallinn </td><td> 01:00:13 </td>
				</tr>
				<tr>
					<td> 4 </td><td> Paula </td><td> Tapa </td><td> 01:02:45 </td>
				</tr>
				<tr>
					<td> 5 </td><td> Kaur  </td><td> Valga </td><td> 01:02:58 </td>
				</tr>
				<tr>
					<td> 6 </td><td> Martin </td><td> Tartu </td><td> 01:06:22 </td>
				</tr>
				<tr>
					<td> 7 </td><td> Liis </td><td> Tallinn </td><td> 01:07:33 </td>
				</tr>
				<tr>
					<td> 8 </td><td> Batman </td><td> Moskva </td><td> 01:09:32 </td>
				</tr>
				<tr>
					<td> 9 </td><td> Sander-Joosep </td><td> Tartu </td><td> 01:10:08 </td>
				</tr>
				<tr>
					<td> 10 </td><td> Mari </td><td> Käru </td><td> 01:58:55 </td>
				</tr>
			</table>
		</div>
<?php elseif($koht=="tallinn") : ?>
            <h1> 1. Tallinna Jooksu Tulemused </h1>
		</div>
		<div id= "section">
			<table>
				<col width="130">
				<col width="200">
				<col width="200">
				<col width="200">
				<tr>
					<th> Võistleja nr. </th><th> Nimi </th><th> Elukoht </th><th> Finišh </th>
				</tr>
				<tr>
					<td> 1 </td><td> Maali </td><td> Tallinn </td><td> 00:56:08 </td>
				</tr>
				<tr>
					<td> 2 </td><td> Jürka </td><td> Pärnu  </td><td> 00:58:32 </td>
				</tr>
				<tr>
					<td> 3 </td><td> Lembit </td><td> Tallinn </td><td> 01:00:13 </td>
				</tr>
				<tr>
					<td> 4 </td><td> Nandu </td><td> Tapa </td><td> 01:02:45 </td>
				</tr>
				<tr>
					<td> 5 </td><td> Vello </td><td> Viljandi </td><td> 01:02:58 </td>
				</tr>
				<tr>
					<td> 6 </td><td> Toomas </td><td> Tartu </td><td> 01:06:22 </td>
				</tr>
				<tr>
					<td> 7 </td><td> Ralf </td><td> Tallinn </td><td> 01:07:33 </td>
				</tr>
				<tr>
					<td> 8 </td><td> Cora </td><td> Portugal </td><td> 01:09:32 </td>
				</tr>
				<tr>
					<td> 9 </td><td> Erich </td><td> Saksamaa </td><td> 01:10:08 </td>
				</tr>
				<tr>
					<td> 10 </td><td> Jose </td><td> Hispaania </td><td> 01:58:55 </td>
				</tr>
			</table>
		</div>
<?php elseif($koht=="viljandi"): ?>
            <h1> 1. Viljandi Jooksu Tulemused </h1>
		</div>
		<div id= "section">
			<h1> Tulemusi ei ole veel </h1>
		</div>
<?php elseif($koht=="parnu") : ?>
        	<h1> 1. Pärnu Jooksu Tulemused </h1>
		</div>
		<div id= "section">
			<h1> Tulemusi ei ole veel </h1>
		</div>
<?php elseif($koht=="rakvere") : ?>
            <h1> 1. Rakvere Jooksu Tulemused </h1>
		</div>
		<div id= "section">
			<h1> Tulemusi ei ole veel </h1>
		</div>
<?php else: ?>
        <h1> Tulemused </h1>
    </div>
    <div id="section">
        <ul>
            <li><a href = "?mode=tulemused&koht=tartu">Tartu Jookseb 2015</a></li>
            <li><a href = "?mode=tulemused&koht=tallinn">Tallinn Jookseb 2015</a></li>
            <li><a href = "?mode=tulemused&koht=parnu">Pärnu Jookseb 2015</a></li>
            <li><a href = "?mode=tulemused&koht=viljandi">Viljandi jookseb 2015</a></li>
            <li><a href = "?mode=tulemused&koht=rakvere">Rakvere jookseb 2015</a></li>
        </ul>
    </div>

<?php endif; ?>