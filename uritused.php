<?php 
    if(isset($_GET['koht'])) {
        $koht = $_GET['koht'];
    } else {
        $koht = "";
    }
?>

<?php if($koht=="tartu") : ?>
            <h1> Tartu Jooks 2015 </h1>
		</div>
		<p> Eestimaa Jookseb 2015 - Tartu sarjas on kavas  nii täispikk 42km distants kui ka 21km pikkune poolmaraton. Võimalik on läbida ka 10km pikkune jooks.  
		Tartu maratonid on Eestis unikaalsed jooksud, kuna distants joostakse ühel ringil ja suurem osa sellest linna südames.<p>
			<img src="Pildid/tartu.jpg" width="800" height="300" alt="Tartu" />
<?php elseif($koht=="tallinn") : ?>
           <h1> Tallinna Jooks 2015 </h1>
		</div>
		<p> Eestimaa Jookseb 2015 -  Tallinna sarjas on kavas nii täispikk 42km distants kui ka 21km pikkune poolmaraton. Võimalik on läbida ka 10km pikkune jooks. 
		Tallinna maratonid on Eestis unikaalsed jooksud, kuna distants joostakse ühel ringil ja suurem osa sellest linna südames<p>
			<img src="Pildid/Tallinn.jpg" width="800" height="300" alt="Tallinn"/>
            
<?php elseif($koht=="viljandi"): ?>
           <h1> Viljandi Jooks 2015 </h1>
		</div>
		<p> Eestimaa Jookseb 2015 - Viljandi sarjas on kavas nii täispikk 42km distants kui ka 21km pikkune poolmaraton. Võimalik on läbida ka 10km pikkune jooks. 
		Viljandi maratonid on Eestis unikaalsed jooksud, kuna distants joostakse ühel ringil ja suurem osa sellest linna südames <p>
			<img src="Pildid/Viljandi.jpg" width="800" height="300" alt="Viljandi" />
            
<?php elseif($koht=="parnu") : ?>
       	<h1> Pärnu Jooks 2015 </h1>
		</div>
		<p> Eestimaa Jookseb 2015 - Pärnu sarjas on kavas nii täispikk 42km distants kui ka 21km pikkune poolmaraton. Võimalik on läbida ka 10km pikkune jooks. 
			Pärnu maratonid on Eestis unikaalsed jooksud, kuna distants joostakse ühel ringil ja suurem osa sellest linna südames. <p>
			<img src="Pildid/Pärnu.jpeg" width="800" height="300" alt = "Pärnu" />
        	
<?php elseif($koht=="rakvere") : ?>
          <h1> Rakvere Jooks 2015 </h1>

		</div>
		<p>  Eestimaa Jookseb 2015 - Rakvere sarjas on kavas nii täispikk 42km distants kui ka 21km pikkune poolmaraton. Võimalik on läbida ka 10km pikkune jooks. 
		Rakvere maratonid on Eestis unikaalsed jooksud, kuna distants joostakse ühel ringil ja suurem osa sellest linna südames.<p>
			<img src="Pildid/Rakvere.jpg" width="800" height="300" alt="Rakvere" />
    
        
<?php else: ?>
        <h1> Üritused </h1>
    </div>
    <div id="section">
        <ul>
            <li><a href="?mode=uritused&koht=tartu"> Tartu Jooks 2015</a></li>
            <li><a href="?mode=uritused&koht=tallinn">Tallinna jooks 2015</a></li>
            <li><a href="?mode=uritused&koht=parnu">Pärnu Jooks 2015</a></li>
            <li><a href="?mode=uritused&koht=viljandi">Viljandi Jooks 2015</a></li>
            <li><a href="?mode=uritused&koht=rakvere">Rakvere Jooks 2015</a></li>
        </ul>
    </div>
<?php endif; ?>
