<?php

function alusta_sessioon(){
	session_start();
}
	
function lopeta_sessioon(){
	session_start();
    if(!empty($_SESSION['logitud'])) {
        $_SESSION['logitud']="";
        session_destroy();
    }
    header("Location: ../index.php");
}
?>